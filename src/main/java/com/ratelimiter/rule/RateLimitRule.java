package com.ratelimiter.rule;

import com.ratelimiter.bucket.RefillInfo;

public class RateLimitRule {

  private final int maxBucketSize;
  private final RefillInfo refillInfo;

  public RateLimitRule(int maxBucketSize, RefillInfo refillInfo) {
    this.maxBucketSize = maxBucketSize;
    this.refillInfo = refillInfo;
  }

  public int getMaxBucketSize() {
    return maxBucketSize;
  }

  public RefillInfo getRefillInfo() {
    return refillInfo;
  }
}
