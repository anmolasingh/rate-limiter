package com.ratelimiter.rule;

import java.util.HashMap;
import java.util.Map;

public class RateLimitRuleHolder {

  private final Map<String, RateLimitRule> rateLimitRuleMap;

  public RateLimitRuleHolder() {
    this.rateLimitRuleMap = new HashMap<>();
  }

  public RateLimitRule get(String userId) {
    return rateLimitRuleMap.get(userId);
  }

  public void put(String userId, RateLimitRule rule){
    rateLimitRuleMap.put(userId, rule);
  }
}
