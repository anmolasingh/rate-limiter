package com.ratelimiter.cache;

import com.ratelimiter.bucket.TokenBucket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TokenBucketCache {

  private final Map<String, TokenBucket> tokenBucketMap;

  public TokenBucketCache() {
    this.tokenBucketMap = new ConcurrentHashMap<>();
  }

  public TokenBucket get(String key) {
    return tokenBucketMap.get(key);
  }

  public void put(String key, TokenBucket bucket){
    tokenBucketMap.put(key, bucket);
  }
}
