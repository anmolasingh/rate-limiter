package com.ratelimiter.bucket;

import java.util.concurrent.TimeUnit;

public class RefillInfo {

  public RefillInfo(double refillRate, TimeInfo timeInfo) {
    if(refillRate <= 0){
      throw new IllegalArgumentException("Refill rate should be a positive value.");
    }
    if(timeInfo == null || timeInfo.timeUnit == null || timeInfo.timeValue <= 0){
      throw new IllegalArgumentException("Incorrect refill time information.");
    }
    this.refillRate = refillRate;
    this.timeInfo = timeInfo;
  }

  private final double refillRate;
  private final TimeInfo timeInfo;

  public double getRefillRate() {
    return refillRate;
  }

  public TimeInfo getTimeInfo() {
    return timeInfo;
  }

  public static class TimeInfo{
    private final TimeUnit timeUnit;
    private final long timeValue;

    public TimeInfo(TimeUnit timeUnit, long timeValue) {
      this.timeUnit = timeUnit;
      this.timeValue = timeValue;
    }

    public TimeUnit getTimeUnit() {
      return timeUnit;
    }

    public long getTimeValue() {
      return timeValue;
    }
  }

}
