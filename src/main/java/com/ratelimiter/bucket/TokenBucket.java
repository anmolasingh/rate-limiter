package com.ratelimiter.bucket;

import com.ratelimiter.bucket.RefillInfo.TimeInfo;

public class TokenBucket {

  private final long maxBucketSize;
  private final double refillRatePerMillisecond;

  private double currentBucketSize = 1;
  private long lastRefillTimestamp;

  public TokenBucket(long maxBucketSize, RefillInfo refillInfo) {
    this.maxBucketSize = maxBucketSize;
    TimeInfo timeInfo = refillInfo.getTimeInfo();
    this.refillRatePerMillisecond =
        refillInfo.getRefillRate() / timeInfo.getTimeUnit().toMillis(timeInfo.getTimeValue());
    lastRefillTimestamp = System.currentTimeMillis();
  }

  public synchronized boolean isRequestAllowed(int tokens) {
    refill();

    if (currentBucketSize >= tokens) {
      currentBucketSize -= tokens;
      return true;
    }
    return false;
  }

  private void refill() {
    long now = System.currentTimeMillis();
    double tokensToAdd = (now - lastRefillTimestamp) * refillRatePerMillisecond;
    currentBucketSize = Math.min(currentBucketSize + tokensToAdd, maxBucketSize);
    lastRefillTimestamp = now;
  }
}
