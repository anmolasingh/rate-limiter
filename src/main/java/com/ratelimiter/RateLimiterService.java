package com.ratelimiter;

import com.ratelimiter.bucket.TokenBucket;
import com.ratelimiter.cache.TokenBucketCache;
import com.ratelimiter.rule.RateLimitRule;
import com.ratelimiter.rule.RateLimitRuleHolder;

public class RateLimiterService {

  private final TokenBucketCache cache;
  private final RateLimitRuleHolder ruleHolder;

  public RateLimiterService(RateLimitRuleHolder ruleHolder) {
    this.ruleHolder = ruleHolder;
    this.cache = new TokenBucketCache();
  }

  public boolean checkLimit(String userId) {
    RateLimitRule rule = ruleHolder.get(userId);
    if(rule != null){
      TokenBucket bucket = cache.get(userId);
      if (bucket == null) {
        bucket = new TokenBucket(rule.getMaxBucketSize(), rule.getRefillInfo());
        cache.put(userId, bucket);
      }
      return bucket.isRequestAllowed(1);
    }
    return true;
  }
}
