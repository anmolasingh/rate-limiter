package com.ratelimiter;

import com.ratelimiter.bucket.RefillInfo;
import com.ratelimiter.bucket.RefillInfo.TimeInfo;
import com.ratelimiter.rule.RateLimitRule;
import com.ratelimiter.rule.RateLimitRuleHolder;
import java.util.concurrent.TimeUnit;
import org.junit.Test;

public class RateLimiterTest {

  @Test
  public void integrationTest() throws InterruptedException {
    RateLimitRuleHolder ruleHolder = new RateLimitRuleHolder();
    ruleHolder.put("user1", new RateLimitRule(5, new RefillInfo(1, new TimeInfo(TimeUnit.SECONDS, 1))));
    ruleHolder.put("user2", new RateLimitRule(10, new RefillInfo(1, new TimeInfo(TimeUnit.HOURS, 1))));
    RateLimiterService service = new RateLimiterService(ruleHolder);
    long startTime = System.currentTimeMillis();
    for (int i=0; i< 10; i++){
      System.out.println(getTimeElapsedInSeconds(startTime) + ":user1:" + service.checkLimit("user1"));
      //System.out.println(getTimeElapsedInSeconds(startTime) + ":user2:" + service.checkLimit("user2"));
    }
    Thread.sleep(TimeUnit.SECONDS.toMillis(10));
    for (int i=0; i< 8; i++){
      System.out.println(getTimeElapsedInSeconds(startTime) + ":user1:" + service.checkLimit("user1"));
    }
  }

  private long getTimeElapsedInSeconds(long startTime) {
    return TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime);
  }
}
